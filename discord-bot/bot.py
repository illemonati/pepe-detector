#!/usr/bin/env python3

import discord
import os
from dotenv import load_dotenv
import requests
from io import BytesIO
from PIL import Image
import tensorflow as tf
import numpy as np
import random

load_dotenv()

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

model = tf.keras.models.load_model(os.environ["MODEL"])
print(model.summary())

pepe_replies = [
    "Please do not use hate symbols.",
    "WARNING This is a christian server.",
    "\"Pepes\" as they are named, are not allowed, on this server!!!",
    "Delete This",
    "DELETE THIS NOW PEPE BANNED REEEEE"
]

wojak_replies = [
    "Gub Muh Soylent.",
    "This is a christian server.",
    "Join the Sharty Gang",
    "SOYYYYYYY",
    "AHHHHHHHHHHHHHHHHH"
]

replies = {
    'pepe': pepe_replies,
    'wojak': wojak_replies
}

def process_attachment_URL(url):
    img = Image.open(BytesIO(requests.get(url).content))
    rgb = img.convert("RGB")
    arr = np.asarray(rgb, dtype='float32')
    normalized = arr / 255.
    resized = tf.keras.preprocessing.image.smart_resize(normalized, (512, 512))
    batched = np.reshape(resized, (1, 512, 512, 3))
    return batched


@client.event
async def on_ready():
    print(f"Logged in as {client.user}")


@client.event
async def on_message(message):
    for attachment in message.attachments:
        if attachment.content_type.startswith('image'):
            processed_img = process_attachment_URL(attachment.proxy_url)
            result = model.predict(processed_img)
            print(result)
            if result[0][2] < 0.5:
                prediction = ['pepe', 'wojak'][np.argmax(result[0][:2])]
                embed = discord.Embed(title=random.choice(replies[prediction]))
                embed.set_footer(text=f"Pepe: {result[0][0]:.1%} | Wojak: {result[0][1]:.1%} | Neither: {result[0][2]:.1%}. Powered by DeepPepe 0.0.1")
                await message.reply(embed=embed)




client.run(os.environ['PEPE_DETECTOR_BOT_TOKEN'])